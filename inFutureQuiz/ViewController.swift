//
//  ViewController.swift
//  inFutureQuiz
//
//  Created by Øystein Tetlie Berg on 29.11.14.
//  Copyright (c) 2014 inFuture AS. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    lazy var managedObjectContext : NSManagedObjectContext? = {
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if let managedObjectContext = appDelegate.managedObjectContext {
            return managedObjectContext
        }
        else {
            return nil
        }
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println(managedObjectContext!)
        // Do any additional setup after loading the view, typically from a nib.
        
//        let newItem = NSEntityDescription.insertNewObjectForEntityForName("QuizFacts", inManagedObjectContext: self.managedObjectContext!) as QuizFacts
        
        
        func presentItemInfo() {
            let fetchRequest = NSFetchRequest(entityName: "QuizFacts")
            if let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: nil) as? [QuizFacts] {
                
                let alert = UIAlertView()
                alert.title = fetchResults[0].statement
                alert.message = fetchResults[0].person
                alert.show()
            }
        }
        
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 1, statement: "spiller i band", person: "Olav")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 2, statement: "spilte i band med Jokke", person: "Nikolai")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 3, statement: "har gitt ut filmquizbok", person: "Camilla")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 4, statement: "er lidenskapelig sopp-plukker", person: "Camilla")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 5, statement: "bodde i USA som barn", person: "Kari")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 6, statement: "driver med paragliding", person: "Erik")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 7, statement: "brettseiler så ofte som mulig", person: "Øystein")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 8, statement: "fridde i sovepose", person: "Robert")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 9, statement: "Bjørn fun-fact 1", person: "Bjørn")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 10, statement: "Bjørn fun-fact 2", person: "Bjørn")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 11, statement: "Bjørn fun-fact 3", person: "Bjørn")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 12, statement: "Bjørn fun-fact 4", person: "Bjørn")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 13, statement: "Bjørn fun-fact 5", person: "Bjørn")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 14, statement: "Svein fun-fact 1", person: "Svein")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 15, statement: "Svein fun-fact 2", person: "Svein")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 16, statement: "Svein fun-fact 3", person: "Svein")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 17, statement: "Svein fun-fact 4", person: "Svein")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 18, statement: "Svein fun-fact 5", person: "Svein")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 19, statement: "Olav fun-fact 2", person: "Olav")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 20, statement: "Olav fun-fact 3", person: "Olav")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 21, statement: "Olav fun-fact 4", person: "Olav")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 22, statement: "Olav fun-fact 5", person: "Olav")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 23, statement: "Kari fun-fact 2", person: "Kari")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 24, statement: "Kari fun-fact 3", person: "Kari")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 25, statement: "Kari fun-fact 4", person: "Kari")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 26, statement: "Kari fun-fact 5", person: "Kari")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 27, statement: "Camilla fun-fact 3", person: "Camilla")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 28, statement: "Camilla fun-fact 4", person: "Camilla")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 29, statement: "Camilla fun-fact 5", person: "Camilla")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 30, statement: "Nikolai fun-fact 2", person: "Nikolai")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 31, statement: "Nikolai fun-fact 3", person: "Nikolai")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 32, statement: "Nikolai fun-fact 4", person: "Nikolai")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 33, statement: "Nikolai fun-fact 5", person: "Nikolai")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 34, statement: "Robert fun-fact 2", person: "Robert")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 35, statement: "Robert fun-fact 3", person: "Robert")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 36, statement: "Robert fun-fact 4", person: "Robert")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 37, statement: "Robert fun-fact 5", person: "Robert")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 38, statement: "Erik fun-fact 2", person: "Erik")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 39, statement: "Erik fun-fact 3", person: "Erik")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 40, statement: "Erik fun-fact 4", person: "Erik")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 41, statement: "Erik fun-fact 5", person: "Erik")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 42, statement: "Øystein fun-fact 2", person: "Øystein")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 43, statement: "Øystein fun-fact 3", person: "Øystein")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 44, statement: "Øystein fun-fact 4", person: "Øystein")
        QuizFacts.createInManagedObjectContext(self.managedObjectContext!, id: 45, statement: "Øystein fun-fact 5", person: "Øystein")
        
        presentItemInfo()
    }

    func randomFact() -> [String: String] {
            var quizFactsCount = UInt32(QuizFacts.count)
            var unsignedRandomNumber = arc4random_uniform(quizFactsCount)
            var randomNumber = Int(unsignedRandomNumber)
            var randomStatement = managedObjectContext?.executeFetchRequest(FetchStatement, error: <#NSErrorPointer#>)
            return [randomNumber]
            
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

