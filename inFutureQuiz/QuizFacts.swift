//
//  QuizFacts.swift
//  inFutureQuiz
//
//  Created by Øystein Tetlie Berg on 29.11.14.
//  Copyright (c) 2014 inFuture AS. All rights reserved.
//

import Foundation
import CoreData

class QuizFacts: NSManagedObject {

    @NSManaged var id: NSNumber
    @NSManaged var statement: String
    @NSManaged var person: String
    class func createInManagedObjectContext(moc: NSManagedObjectContext, id: NSNumber, statement: String, person: String) -> QuizFacts {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("QuizFacts", inManagedObjectContext: moc) as QuizFacts
        newItem.id = id
        newItem.statement = statement
        newItem.person = person
        
        return newItem
    }
}
